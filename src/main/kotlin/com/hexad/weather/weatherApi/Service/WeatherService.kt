package com.hexad.weather.weatherApi.Service

import com.hexad.weather.weatherApi.model.Weather

interface WeatherService{
    fun create(weather: Weather?): Weather
}