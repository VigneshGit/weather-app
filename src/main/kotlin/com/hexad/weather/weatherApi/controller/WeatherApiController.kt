package com.hexad.weather.weatherApi.controller

import com.hexad.weather.weatherApi.Service.WeatherService
import com.hexad.weather.weatherApi.exception.DuplicateWeatherDataException
import com.hexad.weather.weatherApi.model.Weather
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class WeatherApiController(val weatherService: WeatherService){

    @PostMapping(value = "/weather")
    @ResponseStatus(HttpStatus.CREATED)
    @Throws(DuplicateWeatherDataException::class)
    fun createWeather(@Valid @RequestBody weather: Weather): Weather{
        return weatherService.create(weather)
    }

}