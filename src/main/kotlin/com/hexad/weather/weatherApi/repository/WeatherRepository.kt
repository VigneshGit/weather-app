package com.hexad.weather.weatherApi.repository

import com.hexad.weather.weatherApi.model.Weather
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface WeatherRepository : JpaRepository<Weather, Long>{

}