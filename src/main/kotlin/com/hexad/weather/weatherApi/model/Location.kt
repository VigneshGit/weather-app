package com.hexad.weather.weatherApi.model

import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class Location{

    @Column(name = "city")
    var city: String? = null

    @Column(name = "state")
    var state: String? = null

    @Column
    var latitude: Float? = null

    @Column
    var longitude: Float? = null

    constructor(){}

    constructor(city: String, state: String, latitude: Float, longitude: Float){
        this.city = city
        this.state = state
        this.latitude = latitude
        this.longitude = longitude
    }

}