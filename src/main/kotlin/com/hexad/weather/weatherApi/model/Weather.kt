package com.hexad.weather.weatherApi.model

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table( name = "weatherData", uniqueConstraints = arrayOf(UniqueConstraint(columnNames = arrayOf("id"))))
class Weather{

    @Id
    var id: Long = -1

    @Column(name="date_recorded",nullable = false)
    @NotNull(message = "Date recorded not be null!")
    @JsonFormat(pattern = "yyyy-MM-dd")
    var dateRecorded : Date? = null

    @Column(nullable = false)
    @NotNull(message = "Location can not be null!")
    @Embedded
    var location: Location? = null

    @NotNull(message = "Temperature can not be null!")
    @Column(name = "temperature_values", nullable = false)
    var temperatureValues: String? = null

    constructor(){}

    constructor(id: Long, dateRecorded: Date, location: Location, temperatureValues: String){
        this.id = id
        this.dateRecorded = dateRecorded
        this.location = location
        this.temperatureValues = temperatureValues
    }
}