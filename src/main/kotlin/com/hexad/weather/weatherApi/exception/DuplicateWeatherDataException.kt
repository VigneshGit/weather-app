package com.hexad.weather.weatherApi.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Duplicate Weather Data")
class DuplicateWeatherDataException: Exception(){

}