package com.hexad.weather.weatherApi.ServiceImpl

import com.hexad.weather.weatherApi.Service.WeatherService
import com.hexad.weather.weatherApi.exception.DuplicateWeatherDataException
import com.hexad.weather.weatherApi.model.Weather
import com.hexad.weather.weatherApi.repository.WeatherRepository
import org.springframework.stereotype.Service


@Service
class WeatherServiceImpl(val weatherRepository: WeatherRepository): WeatherService{

    @Throws(DuplicateWeatherDataException::class)
    override fun create(weather: Weather?): Weather {
        if (weatherRepository.findById(weather?.id) != null)
            throw DuplicateWeatherDataException()
        return weatherRepository.save(weather!!)
    }
}