package com.hexad.weather.weatherApi.controller

import com.hexad.weather.weatherApi.Service.WeatherService
import com.hexad.weather.weatherApi.model.Location
import com.hexad.weather.weatherApi.model.Weather
import com.jayway.jsonpath.spi.json.GsonJsonProvider
import org.apache.catalina.mapper.Mapper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import java.text.SimpleDateFormat
import java.util.*
import com.fasterxml.jackson.databind.ObjectMapper
import org.assertj.core.api.Assertions
import org.springframework.http.HttpStatus
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.context.TestPropertySource


@RunWith(MockitoJUnitRunner::class) // MockitoJUnitRunner class to initialize the test data eg) @Before
class WeatherApiRestControllerTest{

    private val WEATHERS_ENDPOINT = "/weather"

    lateinit var mockMvc: MockMvc

    @InjectMocks // Used to Create and Inject the mock objects
    lateinit var weatherApiController: WeatherApiController

    private val mapper = ObjectMapper()

    @Mock // Used to create mock objects to be injected
    lateinit var weatherService: WeatherService

    @Before
    fun setup(){
        //MockitoAnnotations.initMocks(WeatherApiController::class)
        mockMvc = MockMvcBuilders.standaloneSetup(weatherApiController).build()
    }

    @Test
    @Throws(Exception::class)
    fun shouldCreateWeatherDataForValidWeatherDataObject(){
        val expectedWeather : Weather = createWeatherDataObject()
        Mockito.`when`(weatherService.create(ArgumentMatchers.isA(Weather::class.java))).thenReturn(expectedWeather)
        val expectedWeatherAsString = mapper.writeValueAsString(expectedWeather)
        val mvcResponse: MockHttpServletResponse = mockMvc.perform(MockMvcRequestBuilders.post(WEATHERS_ENDPOINT, expectedWeather).contentType(MediaType.APPLICATION_JSON)
                .content(expectedWeatherAsString))
                .andReturn().response

        Assertions.assertThat(mvcResponse.status).isEqualTo(HttpStatus.CREATED.value())
        Mockito.verify(weatherService, Mockito.times(1)).create(ArgumentMatchers.isA(Weather::class.java))
        Mockito.verifyNoMoreInteractions(weatherService)
    }

    private fun createWeatherDataObject() : Weather{
        var expectedWeather: Weather = Weather()
        expectedWeather.id = 1L
        expectedWeather.dateRecorded = Date()
        expectedWeather.location = Location("Chennai", "TamilNadu", 10f, 20f)
        expectedWeather.temperatureValues = "11, 12"
        return expectedWeather
    }


}