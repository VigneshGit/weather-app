package com.hexad.weather.weatherApi.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.hexad.weather.weatherApi.WeatherApiApplication
import com.hexad.weather.weatherApi.model.Location
import com.hexad.weather.weatherApi.model.Weather
import org.assertj.core.api.Assertions
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import java.util.*


@RunWith(SpringRunner::class) //To Create the Spring Context
@SpringBootTest(classes = arrayOf(WeatherApiApplication::class), webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = arrayOf("classpath:application-test.properties"))
class WeatherApiRestControllerIntegrationTest{

    @LocalServerPort //Once Spring boot assigned a port, same assigned automatically
    var port: Int = 0

    @Autowired
    private lateinit var mockMvc: MockMvc

    private val WEATHERS_ENDPOINT = "/weather"
    private val mapper = ObjectMapper()

    @Test
    @Throws(Exception::class)
    fun shouldCreateWeatherDataForValidWeatherDataObject(){
        val expectedWeather : Weather = createWeatherDataObject()
        val expectedWeatherAsString = mapper.writeValueAsString(expectedWeather)
        val mvcResponse: MockHttpServletResponse = mockMvc.perform(MockMvcRequestBuilders.post(WEATHERS_ENDPOINT, expectedWeather)
                .contentType(MediaType.APPLICATION_JSON)
                .content(expectedWeatherAsString))
                .andReturn().response

        Assertions.assertThat(mvcResponse.status).isEqualTo(HttpStatus.CREATED.value())
    }

    fun createWeatherDataObject(): Weather {
        val expectedWeather: Weather = Weather()
        expectedWeather.id = 1L
        expectedWeather.dateRecorded = Date()
        expectedWeather.location = Location("Chennai", "TamilNadu", 10f, 20f)
        expectedWeather.temperatureValues = "11, 12"
        return expectedWeather
    }
}