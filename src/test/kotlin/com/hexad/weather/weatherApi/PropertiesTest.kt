import junit.framework.TestCase
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class) //To Create the Spring Context
@TestPropertySource(locations = arrayOf("classpath:application-test.properties"))
class PropertiesTest: TestCase(){

    @Autowired
    lateinit var env: Environment

    @Test
    fun verifyDriverClassNameProperty(){
        Assert.assertEquals("org.h2.Driver", env.getProperty("spring.datasource.driverClassName"))
    }

    @Test
    fun verifyUrlProperty(){
        Assert.assertEquals("jdbc:h2:mem:weatherDB;DB_CLOSE_DELAY=-1", env.getProperty("spring.datasource.url"))
    }

    @Test
    fun verifyUserNameProperty(){
        Assert.assertEquals("sa", env.getProperty("spring.datasource.username"))
    }

}